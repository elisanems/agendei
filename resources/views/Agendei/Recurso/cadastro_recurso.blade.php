<!DOCTYPE html>

<html lang="pt-br">
    <head>

        <meta charset="utf-8">
        <link href="{{ asset('css/stilo.css') }}" rel="stylesheet">
    </head>


{{--link das funções da rotina--}}
<ul>
    <div class="cart" >
        <label class="scrip"> Recurso </label>
        <a  class="phs" href="{{ url('/recurso') }}">Voltar</a>
    </div>
</ul>


    <body>

@if(isset ($errors) && count($errors) > 0 )
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{$error}}</p>
        @endforeach
    </div>
@endif

<form  method="post" action="{{route('recurso.store')}}">

                <div class="castr">cadastro recurso</div>
            @csrf
        <div class="cardrs">
            <div class="t1">
                 <label>Nome</label><br>
                <input class="p1" type="text" name="nome" required value='{{old('nomne')}}'><br>
            </div>

            <div class="t2">
                <label> Sigla</label><br>
                <input class="p2" type="text" name="sigla" required value='{{old('sigla')}}'><br>
            </div>

            <div class="t3">
                <label> Descrição Completa</label><br>
                <input class="p3" type="text" name="descricao"required value='{{old('descricao')}}'><br>
            </div>

                <div class="oi" >
                <select name="status">
                    <option>Escolha o status do recurso:</option>
                    @foreach($statuss as $status)
                    <option value="{{$status}}"
                    @if( isset($recurso) && $recurso->status == $status)
                    @endif>{{$status}}</option>
                    @endforeach
                </select>
            </div>
               <div class="rs">
                 <input class="rss" type="submit" value="Gravar">
              </div>
            </div>
        </form>
    </body>
</html>
