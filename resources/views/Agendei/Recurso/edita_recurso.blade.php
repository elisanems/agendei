<h3> Cadastro Recurso </h3>

{{--link das funções da rotina--}}
<ul>
    <li>
        <a href="{{ url('/recurso') }}">Voltar</a>
    </li>
</ul>

<!DOCTYPE html>

<html lang="pt-br">
    <head>

        <meta charset="utf-8">
    </head>
    <body>

@if(isset ($errors) && count($errors) > 0 )
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{$error}}</p>
        @endforeach
    </div>
@endif

<form method="post" action="{{route('recurso.update', $recurso->id)}}">


            @csrf
            @method('put')

                Nome: <input type="text" name="nome" value='{{$recurso->nome or old('nomne')}}'><br><br>
                Sigla: <input type="text" name="sigla" value='{{$recurso->sigla or old('sigla')}}'><br><br>
                Descrição Completa: <input type="text" name="descricao" value='{{$recurso->descricao or old('descricao')}}'><br><br>

                <select name="status">
                    <option>Escolha o status do recurso:</option>
                    @foreach($statuss as $status)
                    <option>
                     value="{{$status}}"
                    @if( isset($recurso) && $recurso->status == $status)
                    @endif
                    {{$status}}</option>
                    @endforeach
                </select>

                <div style="text-align: right">
                <input type="submit" value="Gravar">
              </div>
            </p>
        </form>
    </body>
</html>
