<!DOCTYPE html>

<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <link href="{{ asset('css/stilo.css') }}" rel="stylesheet">
        <title>cadastros de funcionario</title>
    </head>

<ul>
    <div class="tell">
        <label class="r">Tela de Cadastro</label>
        <a class="ui" href="{{ url('/funcionario') }}">voltar</a>
    </div>
</ul>

    <body id="fund">

@if(isset ($errors) && count($errors) > 0 )
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{$error}}</p>
        @endforeach
    </div>
@endif

<form  method="post" action={{route('funcionario.store')}}>
            @csrf

                <div class="registroformulario">Formulario de cadastro</div>
             <div class="telcard">
                <div class="ar6">
                <label class="non1"> Nome</label><br>
                <input class="non" type="text" name="nome" placeholder="digite seu nome " value="{{old('nome')}}"><br>
                </div>

                <div class="ar5">
                <label class="cp1" > CPF</label><br>
                <input class="cp" type="number" name="CPF"  placeholder="Digite seu cpf" value="{{old('CPF')}}"><br>
                </div>

                <div class="ar4">
                <label > Email</label><br>
                <input class="eml" type="text" name="email" placeholder="digite seu email" value="{{old('email')}}"><br>
                </div>

                <div class="ar3">
                <label class="telf1" > Telefone</label><br>
                <input class="telf" type="number" name="telefone" placeholder="digite seu telefone" value="{{old('telefone')}}"><br>
                </div>

                <div class="ar2">
                <label class="ltlogin1" >Login</label><br>
                <input class="ltlogin" type="text"name="login" placeholder="digite seu login" value="{{old('login')}}"><br>
                </div>

                <div class="ar">
                 <label class="ltsenha1">Senha</label><br>
                 <input class="ltsenha" type="password"  name="senha" placeholder="digite sua senha " value="{{old('senha')}}"><br><br>
                </div>

                <div class="nometipo">

                    <select name="tipo">
                    <option >Escolha o tipo:</option>
                        @foreach($tipos as $tipo)
                    <option value="{{$tipo}}"
                    @if( isset($funcionario) && $funcionario->tipo == $tipo)
                            selected;
                        @endif
                        >{{$tipo}}</option>
                    @endforeach
                </select>

                <select name="status">
                    <option>Escolha o status do funcionario:</option>
                        @foreach($statuss as $status)
                    <option value="{{$status}}"
                    @if( isset($funcionario) && $funcionario->status == $status)
                            selected
                        @endif
                    >{{$status}}</option>
                    @endforeach
                </select>

            </div>
            <div class="grav">
                <input  class="gravar" type="submit" value="Gravar">
            </div>
            </div>


        </form>
    </body>
</html>
