
<!DOCTYPE html>

<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>Funcionarios</title>
        <link href="{{ asset('css/stilo.css') }}" rel="stylesheet">
    </head>
    <body id="fund">


<ul>
<div class="divfui">
        <label class="ff">funcionario</label>
        <a class="volt" href="{{ url('/menuprincipal') }}">Voltar</a>
        <a class="cadast" href="{{route('funcionario.create')}}" role="button">Cadastrar</a>

    </div>
</ul>


 <div class="tab">
         <table class="tabelas1">
        <tr>

            <th class="id1">ID</th>
            <th class="nome1">NOME</th>
            <th class="cpf1">CPF</th>
            <th class="telefone1">TELEFONE</th>
            <th class="email1">EMAIL</th>
            <th class="tipo1">TIPO</th>
            <th class="status1">STATUS</th>
            <th class="acao1">AÇÃO</th>

        </tr>
    </div>

         @foreach($funcionarios as $funcionario)

         <tr>

            <td class="id1">{{$funcionario->id}}</td>
            <td class="nome1">{{$funcionario->nome}}</td>
            <td class="cpf1">{{$funcionario->CPF}}</td>
            <td class="telefone1">{{$funcionario->telefone}}</td>
            <td class="email1">{{$funcionario->email}}</td>
            <td  class="tipo1">{{$funcionario->tipo}}</td>
            <td class="status1">{{$funcionario->status}}</td>
            <td>
                <a class="ac" href="{{route('funcionario.edit', $funcionario->id)}}" role="button"><?php include "img/editar.svg";?></a>
            </td>
        </tr>
        @endforeach
    </table>

    @php
    $db = mysqli_connect("localhost","root","mysql123","agendei");
    @endphp

    </body>
</html>
