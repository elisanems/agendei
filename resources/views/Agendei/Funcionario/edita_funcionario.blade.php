<!DOCTYPE html>

<html lang="pt-br">
    <head>

        <meta charset="utf-8">
            <link href="{{ asset('css/stilo.css') }}" rel="stylesheet">
    </head>
    <body>

        <ul>
            <div class="tell">

                <label class="r">editar</label>
                <a class="ui" href="{{ url('/funcionario') }}">voltar</a>
            </div>
        </ul>

@if(isset ($errors) && count($errors) > 0 )
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{$error}}</p>
        @endforeach
    </div>
@endif

<form method="post" action="{{route('funcionario.update', $funcionario->id)}}">
            @csrf
            @method('put')

                <div class="registroformulario">Formulario de editar</div>
         <div class="telcard">

            <div class="ar6">
                <label class="an1"> Nome </label><br>
                <input class="an"type="text" name="nome" value="{{$funcionario->nome or old('nome')}}"><br>
                 </div>

                <div class="ar5">
                <label class="acp1">CPF</label><br>
                <input class="acp" type="number" name="CPF" value="{{$funcionario->CPF or old('CPF')}}"><br>
                </div>

                <div class="ar4">
                <label class="am1">Email</label><br>
                <input class="am" type="text" name="email" value="{{$funcionario->email or old('email')}}"><br>
                </div>

                <div class="ar3">
                <label class="at1"> Telefone</label><br>
                <input class="at" type="number" name="telefone" value="{{$funcionario->telefone or old('telefone')}}"><br>
                </div>

                <div class="ar2">
                <label class="al1">Login</label><br>
                <input class="al" type="text" name="login" value="{{$funcionario->login or old('login')}}"><br>
                </div>

                <div class="ar">
                <label class="as1"> Senha</label><br>
                <input class="as" type="password" name="senha" value="{{$funcionario->senha or old('senha')}}"><br><br>
                </div>

                <div class="bott2">

                    <select name="tipo">
                    <option>Escolha o tipo:</option>
                        @foreach($tipos as $tipo)
                    <option value="{{$tipo}}"
                    @if( isset($funcionario) && $funcionario->tipo == $tipo)
                            selected;
                        @endif
                        >{{$tipo}}</option>
                    @endforeach
                </select>
                <select name="status">
                    <option>Escolha o status do funcionario:</option>
                        @foreach($statuss as $status)
                    <option value="{{$status}}"
                    @if( isset($funcionario) && $funcionario->status == $status)
                            selected
                        @endif
                    >{{$status}}</option>
                    @endforeach
                </select>
                </div>

               <div class="grav">
                <input class="gravar" type="submit" value="Gravar">
               </div>
        </form>
    </body>
</html>
