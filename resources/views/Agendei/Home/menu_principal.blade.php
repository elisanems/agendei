<!DOCTYPE html>
<html>
<head>
    <title> menu principal </title>
    <link href="{{ asset('css/stilo.css') }}" rel="stylesheet">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
</head>
<body>

    <nav>
        <input type="checkbox" id="check">
        <label for="check" class="checkbtn">
            <i class="fas fa-bars"></i>
        </label>
        <label class="logo">Agendei!</label>

        <ul>
           <li><a class="po" href="{{url('/funcionario') }}">Funcionário</a></li>
           <li><a class="po" href="{{ url('/recurso') }}">Recurso</a></li>
            <li><a class="po" href="{{ url('/agendamento') }}">Agendamento</a></li>
            <li><a  class="po" href="{{ url('/relatorio') }}"> Relatório</a></li>
            <li><a class="active" href="{{ url('/') }}">Sair</a></li>

        </ul>

    </nav>
   <img class="logopng" src="img/logo.jpeg">
</body>
</html>


{{--link das rotinas--}}
