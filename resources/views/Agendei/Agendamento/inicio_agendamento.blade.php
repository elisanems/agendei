
<!DOCTYPE html>

<html lang="pt-br">
    <head>
        <title>Agendamentos</title>
        <meta charset="utf-8">
        <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

    </head>

<ul>
        <div class="agen">
        <label class="bb">Agendamento </label>
        <a class="" href="{{route('agendamento.create')}}" role="button">Cadastrar</a><br><br>
        <a href="{{ url('/menuprincipal') }}">Voltar</a>
        </div>
</ul>


    <body>

    <table class="table table-striped">
        <tr>
            <th>ID</th>
            <th>SOLICITANTE</th>
            <th>RECURSO</th>
            <th>DATA INICIAL</th>
            <th>HORA INICIAL</th>
            <th>DATA FINAL</th>
            <th>HORA FINAL</th>
            <th>OBSERVAÇÃO</th>
            <th>STATUS</th>
            <th>MOTIVO CANCELADO</th>
            <th>AÇÃO</th>
        </tr>
         @foreach($agendamentos as $agendamento)
            <tr>
            <td>{{$agendamento->id}}</td>
            <td>{{$agendamento->id_solicitante}}</td>
            <td>{{$agendamento->id_recurso}}</td>
            <td>{{$agendamento->data_inicial}}</td>
            <td>{{$agendamento->hora_inicial}}</td>
            <td>{{$agendamento->data_final}}</td>
            <td>{{$agendamento->hora_final}}</td>
            <td>{{$agendamento->observacao}}</td>
            <td>{{$agendamento->status}}</td>
            <td>{{$agendamento->motivo_cancelado}}</td>
            <td>
                <a class="btn btn-secondary" href="{{route('agendamento.edit', $agendamento->id)}}" role="button">Editar</a>
            </td>
        </tr>
        @endforeach
    </table>

    @php
    $db = mysqli_connect("localhost","root","mysql123","agendei");
    @endphp
    </body>
</html>
