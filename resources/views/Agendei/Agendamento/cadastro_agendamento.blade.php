<h3> Cadastro Agendamento </h3>

<ul>
    <li>
        <a href="{{ url('/agendamento') }}">Voltar</a>
    </li>
</ul>


    <body>

@if(isset ($errors) && count($errors) > 0 )
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{$error}}</p>
        @endforeach
    </div>
@endif

<form  method="post" action="{{route('agendamento.store')}}">

        @csrf
            <p>

                Solicitante: <input type=number name="id_solicitante" value="{{old('id_solicitante')}}"><br><br>
                Recurso: <input type=number name="id_recurso" value="{{old('id_recurso')}}"><br><br>
                Data Inicial: <input type="date" name="data_inicial" value="{{old('data_inicial')}}">
                Hora Inicial: <input type="time" name="hora_inicial" value="{{old('hora_inicial')}}"><br><br>
                Data Final: <input type="date" name="data_final" value="{{old('data_final')}}">
                Hora Final: <input type="time" name="hora_final" value="{{old('hora_final')}}"><br><br>
                Observação: <input type="text" name="observacao" value="{{old('observacao')}}"><br><br>
                Motivo Cancelado:<input type="text" name="motivo_cancelado" value="{{old('motivo_cancelado')}}"><br><br>
                <select name="status">
                    <option>Escolha o status do agendamento:</option>
                        @foreach($statuss as $status)
                    <option value="{{$status}}"
                    @if( isset($agendamento) && $agendamento->status == $status)
                            selected
                        @endif
                    >{{$status}}</option>
                    @endforeach
                </select>
            <div style="text-align: right">
                <input type="submit" value="Gravar">
            </div>
            </p>
        </form>
    </body>
</html>
