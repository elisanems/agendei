<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Agendei\AgendeiController@agendei');
Route::post('/', 'Agendei\AgendeiController@agendei');

Route::get('/menuprincipal', 'Agendei\AgendeiController@menuprincipal');
Route::post('/menuprincipal', 'Agendei\AgendeiController@menuprincipal');

Route::resource('/funcionario', 'Agendei\FuncionarioController');
Route::get('/funcionario/inclusao', 'Agendei\FuncionarioController@cadastrofuncionario');
Route::post('/funcionario/inclusao', 'Agendei\FuncionarioController@cadastrofuncionario');
Route::post('/funcionario/edit{id}', [FuncionarioController::class, 'edit'])->name('funcionarios.edit');
Route::put('/funcionario/{id}', [FuncionarioController::class, 'update'])->name('funcionarios.update');

Route::resource('/recurso', 'Agendei\RecursoController');
Route::get('/recurso/inclusao', 'Agendei\RecursoController@cadastro_recurso');
Route::post('/recurso/inclusao', 'Agendei\RecursoController@cadastro_recurso');
Route::post('/recurso/edit{id}', [RecursoController::class, 'edit'])->name('recursos.edit');
Route::put('/recurso/{id}', [RecursoController::class, 'update'])->name('recursos.update');

Route::resource ('/agendamento', 'Agendei\AgendamentoController');
Route::get('/agendamento/inclusao', 'Agendei\AgendamentoController@cadastroagendamento');
Route::post('/agendamento/inclusao', 'Agendei\AgendamentoController@cadastroagendamento');
Route::post('/agendamento/edit{id}', [AgendamentoController::class, 'edit'])->name('agendamentos.edit');
Route::put('/agendamento/{id}', [AgendamentoController::class, 'update'])->name('agendamentos.update');


