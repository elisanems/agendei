<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recurso extends Model
{
    //dados que o usuario pode inserir
    protected $fillable = ['nome', 'descricao', 'sigla', 'status'];

}


