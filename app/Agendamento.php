<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agendamento extends Model
{
    /*dados que o usuario pode inserir*/
    protected $fillable = ['id_solicitante', 'id_recurso', 'data_inicial', 'data_final', 'hora_inicial', 'hora_final', 'observacao', 'status'];


}
