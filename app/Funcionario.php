<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funcionario extends Model
{
    //dados que o usuario pode inserir
    protected $fillable = ['nome', 'CPF', 'email', 'telefone', 'login', 'senha', 'tipo', 'status'];

}



