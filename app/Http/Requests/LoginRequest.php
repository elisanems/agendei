<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{

    public function rules()
    {
        return [
            'login' => ['required', 'email'],
            'senha' => ['required', 'min:3', 'max:12'],

        ];
    }

    public function messages()
    {
        return [
            'login.required' => 'Campo obrigatório',
            'login.email' => 'Email inválido',
            'senha.required' => 'Campo obrigatório',
            'senha.min' => 'Campo deve ter no mínimo :min caracteres',
            'login.max' => 'Campo deve ter no máximo :max caracteres',

        ];
    }
}
