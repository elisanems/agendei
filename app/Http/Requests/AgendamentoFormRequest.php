<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AgendamentoFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $rules = [
            'id_solicitante'=> 'required',
            'id_recurso'    => 'required',
            'data_inicial'  => 'required',
            'data_final'    => 'required',
            'hora_inicial'  => 'required',
            'hora_final'    => 'required',
            'observacao'    => 'max:200',
            'status'        => 'required',
        ];
    }

    public function mensagens()
    {
        return [
            'id_solicitante.required'=>'O campo Solicitante é obrigatório, informe um código!',
            'id_recurso.required'=>'O campo Recurso é obrigatório, informe um código!',
            'data_inicial'=>'O campo Data Inicial é obrigatório!',
            'data_final.required'=>'O campo Data Final é obrigatório!',
            'hora_inicial.required'=>'O campo Hora Inicial é obrigatório!',
            'hora_final.required'=>'O campo Hora Final é obrigatório!',
            'status.required'=>'O campo Status é obrigatório!'
        ];
    }
}
