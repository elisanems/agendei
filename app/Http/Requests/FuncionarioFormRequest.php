<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FuncionarioFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $rules = [
            'nome'      => 'required|min:3|max:50',
            'CPF'       => 'required|max:11',
            'email'     => 'required|max:50',
            'telefone'  => 'required|max:11',
            'login'     => 'required|max:20',
            'senha'     => 'required|max:10',
            'tipo'      => 'required',
            'status'    => 'required',
        ];
    }

    public function mensagens()
    {
        return [
            'nome.required'=>'O campo Nome é obrigatório!',
            'CPF.required'=>'O campo CPF é obrigatório!',
            'email.required'=>'O campo Email é obrigatório!',
            'telefone.required'=>'O campo Telefone é obrigatório!',
            'login.required'=>'O campo Login é obrigatório!',
            'senha.required'=>'O campo Senha é obrigatório!',
            'tipo.required'=>'O campo Tipo é obrigatório!',
            'status.required'=>'O campo Status é obrigatório!'
        ];
    }
}
