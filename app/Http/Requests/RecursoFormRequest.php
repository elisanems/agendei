<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecursoFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $rules = [
            'nome'      => 'required|min:3|max:30',
            'descricao' => 'required|min:3|max:100',
            'sigla'     => 'required',
            'status'    => 'required',
        ];
    }

    public function mensagens()
    {
        return [
            'nome.required'=>'O campo Nome é obrigatório!',
            'descricao.required'=>'O campo Descrição é obrigatório!',
            'sigla.required'=>'O campo Sigla é obrigatório!',
            'status.required'=>'O campo Status é obrigatório!'
        ];
    }
}
