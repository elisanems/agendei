<?php

namespace App\Http\Controllers\Agendei;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Funcionario;
use App\Http\Requests\FuncionarioFormRequest;
use Symfony\Component\HttpKernel\Event\ViewEvent;

class FuncionarioController extends Controller
{
    private $funcionario;

    public function __construct(Funcionario $funcionario)
    {
        $this->funcionario = $funcionario;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Funcionario $funcionario)
    {
        $funcionarios = $this->funcionario->all();


        return view('agendei.funcionario.inicio_funcionario', compact('funcionarios'));
    }

    public function cadastrofuncionario()
    {
        return view('agendei.funcionario.cadastro_funcionario');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $statuss = ['ativo', 'inativo', 'suspenso'];
        $tipos = ['funcionario', 'professor', 'coordenador'];

        return View('agendei.funcionario.cadastro_funcionario', compact(['statuss','tipos']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FuncionarioFormRequest $request)
    {
      //traz todos os dados do formulario
      $Form_addfunc = $request->except('_token');

      //insere dados na tabela recurso
       $insert = $this->funcionario->create($Form_addfunc);

       if ($insert)
           return redirect()->route('funcionario.index');
       else
           return redirect()->back('funcionario.create');

       //valida os dados
       $this->validate($request, $this->funcionario->rules);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //recupera o cadastro pelo id

        //$funcionario = $this->funcionario->find($id);


        $statuss = ['ativo', 'inativo', 'suspenso'];
        $tipos = ['funcionario', 'professor', 'coordenador'];

        if (!$funcionario = Funcionario::find($id)) {
            return redirect()->back();
        }
        return View('agendei.funcionario.edita_funcionario', compact(['statuss', 'tipos', 'funcionario']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FuncionarioFormRequest $request, $id)
    {
        if (!$funcionario = Funcionario::find($id)) {
            return redirect()->back();
        }

        //dd("Editando o funcionario : {$funcionario->id}");
        $funcionario->update($request->all());
        return redirect()->route('funcionario.index');


           /*valida os dados*/
        $this->validate($request, $this->funcionario->rules);
      }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

