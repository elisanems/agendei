<?php

namespace App\Http\Controllers\Agendei;

use App\Funcionario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;

class AgendeiController extends Controller
{
    /*public function __construct()
    {
        $this->middleware('auth')
            ->only ('menuprincipal');
    }
*/
    public function agendei()
    {
        return view('Agendei.Home.agendei');
    }

    public function menuprincipal()
    {
        return view('Agendei.Home.menu_principal');
    }

    public function Login()
    {
        return view('Agendei.Home.agendei');
    }

    public function validausuario(LoginRequest $request)
    {
        //Validações:
        $request->validated();

        //Verifica Login
        $usuario = trim($request->input('login'));
        $senha = trim($request->input('senha'));

        $usuario = Funcionario::where('login', $usuario)->first();

        //Verifica se existe usuario
        if (!$usuario) {
            echo ("Errro");
            return;
        }

        //Verifica senha
        if (!Hash::check($senha, $usuario->senha)) {
            echo ("Err0rr");
            return view('Agendei.Home.agendei');
        }

        //criar sessão
        echo ("Sessão Iniciada");
    }
}
