<?php

namespace App\Http\Controllers\Agendei;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RecursoFormRequest;
use App\Recurso;
use Symfony\Component\HttpKernel\Event\ViewEvent;

class RecursoController extends Controller
{
    private $recurso;

    public function __construct(Recurso $recurso)
    {
        $this->recurso = $recurso;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Recurso $recurso)
    {
        $recursos = $this->recurso->all();

        return view('agendei.recurso.inicio_recurso', compact('recursos')) ;
    }
    public function cadastrorecurso() {
        return view('Agendei.Recurso.cadastro_recurso');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $statuss = ['ativo', 'inativo', 'manutencao'];

        return View('agendei.recurso.cadastro_recurso', compact('statuss'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RecursoFormRequest $request)
    {
        //traz todos os dados do formulario
       $Form_addrecurso = $request->except('_token');

       //insere dados na tabela recurso
        $insert = $this->recurso->create($Form_addrecurso);

        if ($insert)
            return redirect()->route('recurso.index');
        else
            return redirect()->back('recurso.create');

        //valida os dados
        $this->validate($request, $this->recurso->rules);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //recupera os dados da pesquisa pelo id
        //$recurso = $this->recurso->find($id);

        $titulo = 'Cadastro Novo Recurso';
        $statuss = ['ativo', 'inativo', 'manutencao'];

        if (!$recurso = Recurso::find($id)) {
            return redirect()->back();
        }
        return View('agendei.recurso.edita_recurso', compact(['titulo', 'statuss', 'recurso']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RecursoFormRequest $request, $id)
    {
        if (!$recurso = Recurso::find($id)) {
            return redirect()->back();
        }

        //dd("Editando o recurso : {$recurso->id}");
        $recurso->update($request->all());
        return redirect()->route('recurso.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
