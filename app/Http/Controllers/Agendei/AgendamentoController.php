<?php

namespace App\Http\Controllers\Agendei;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Agendamento;
use App\Http\Requests\AgendamentoFormRequest;
use Symfony\Component\HttpKernel\Event\ViewEvent;

class AgendamentoController extends Controller
{
    private $agendamento;

    public function __construct(Agendamento $agendamento)
    {
        $this->agendamento = $agendamento;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Agendamento $agendamento)
    {
        $agendamentos = $this->agendamento->all();
        return view('Agendei.Agendamento.inicio_agendamento', compact('agendamentos'));
    }

    public function cadastroagendamento() {
        return view('Agendei.Agendamento.cadastro_agendamento');
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $statuss = ['programado', 'ativo', 'inativo', 'cancelado'];


        return View('agendei.agendamento.cadastro_agendamento', compact('statuss'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AgendamentoFormRequest $request)
    {
        //traz todos os dados do formulario
      $Form_addagend = $request->except('_token');

      //insere dados na tabela recurso
       $insert = $this->agendamento->create($Form_addagend);

       if ($insert)
           return redirect()->route('agendamento.index');
       else
           return redirect()->back('agendamento.create');

       //valida os dados
       $this->validate($request, $this->agendamento->rules);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //recupera o cadastro pelo id

        //$agendamento = $this->agendamento->find($id);

        $statuss = ['programado', 'ativo', 'inativo', 'cancelado'];

        if (!$agendamento = Agendamento::find($id)) {
            return redirect()->back();
        }

        return View('agendei.agendamento.edita_agendamento', compact(['statuss', 'agendamento']));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AgendamentoFormRequest $request, $id)
    {
        if (!$agendamento = Agendamento::find($id)) {
            return redirect()->back();
        }

        //dd("Editando o agendamento : {$agendamento->id}");
        $agendamento->update($request->all());
        return redirect()->route('agendamento.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
