<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgendamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agendamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_solicitante');
            $table->integer('id_recurso');
            $table->date('data_inicial');
            $table->date('data_final');
            $table->time('hora_inicial');
            $table->time('hora_final');
            $table->string('observacao', 200);
            $table->enum('status', ['programado','ativo', 'inativo', 'cancelado']);
            $table->string('motivo_cancelado', 200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendamentos');
    }
}
