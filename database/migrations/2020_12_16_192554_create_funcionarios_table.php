<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFuncionariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funcionarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 50);
            $table->char('CPF', 11);
            $table->string('email', 50);
            $table->char('telefone', 11);
            $table->string('login', 20);
            $table->string('senha', 10);
            $table->enum('tipo', ['funcionario', 'professor', 'coordenador']);
            $table->enum('status', ['ativo', 'inativo', 'suspenso']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funcionarios');
    }
}
